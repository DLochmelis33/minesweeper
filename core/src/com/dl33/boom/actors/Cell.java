package com.dl33.boom.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.dl33.boom.exceptions.ExplodeException;

public class Cell extends Actor {

	private boolean opened = false;

	public boolean isOpened() {
		return opened;
	}

	private int value = 0;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	private boolean mined = false;

	public void setMined(boolean mined) {
		this.mined = mined;
	}

	public boolean isMined() {
		return mined;
	}

	private boolean marked = false;

	public boolean isMarked() {
		return marked;
	}

	private final int xCoord;

	public int getXCoord() {
		return xCoord;
	}

	private final int yCoord;

	public int getYCoord() {
		return yCoord;
	}

	private Texture texture;

	public Cell(float x, float y, int xCoord, int yCoord) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;

		texture = new Texture("Cell_normal.png");
		setSize(texture.getWidth(), texture.getHeight());

		setX(x);
		setY(y);
	}

	public void draw(Batch batch, float parentAlpha) {
		batch.draw(texture, getX(), getY(), getWidth(), getHeight());
	}

	public void open() {
		if (marked || opened) {
			return;
		}
		if (mined) {
			opened = true;
			throw new ExplodeException();
		}
		opened = true;
		marked = false;
		texture = new Texture("Cell_" + value + ".png");
	}

	public boolean toggleMark() {
		if (opened) {
			return false;
		}
		if (!marked)
			texture = new Texture("Cell_flag.png");
		else
			texture = new Texture("Cell_normal.png");
		marked = !marked;
		return marked;
	}

	public void dissecure() {
		if (opened && mined) {
			texture = new Texture("Cell_exploded.png");
			return;
		}
		if (mined && marked) {
			texture = new Texture("Cell_correct.png");
			return;
		}
		if (mined) {
			texture = new Texture("Cell_mined.png");
		}
	}

}

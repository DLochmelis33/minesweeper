package com.dl33.boom.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class PlayAgainButton extends Actor {

	private boolean won = false;
	private Texture texture;

	public PlayAgainButton(int x, int y, boolean won) {
		this.won = won;
		texture = new Texture("PlayAgain" + (this.won ? "Won" : "Lost") + ".png");
		setSize(texture.getWidth(), texture.getHeight());
		setX(x);
		setY(y);
	}

	public void highlight() {
		texture = new Texture("PlayAgainHighlighted.png");
	}

	public void lowlight() {
		texture = new Texture("PlayAgain" + (this.won ? "Won" : "Lost") + ".png");
	}

	public void draw(Batch batch, float parentAlpha) {
		batch.draw(texture, getX(), getY(), getWidth(), getHeight());
	}

}

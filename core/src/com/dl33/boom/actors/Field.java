package com.dl33.boom.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Field extends Actor {

	private Texture texture;

	public Field() {
		texture = new Texture("field.png");
		setSize(texture.getWidth(), texture.getHeight());
	}

	public void draw(Batch batch, float parentAlpha) {
		batch.draw(texture, 0, 0, getWidth(), getHeight());
	}

}

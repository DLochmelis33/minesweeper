package com.dl33.boom.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Selection extends Actor {

	private Texture texture;

	public Selection() {
		texture = new Texture("Selection.png");
		setSize(texture.getWidth(), texture.getHeight());
	}

	public void draw(Batch batch, float parentAlpha) {
		batch.draw(texture, getX(), getY(), getWidth(), getHeight());
	}

}

package com.dl33.boom;

import com.badlogic.gdx.ApplicationAdapter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.dl33.boom.actors.Cell;
import com.dl33.boom.stages.EndGameStage;
import com.dl33.boom.stages.FieldStage;

public class MinesweeperGame extends ApplicationAdapter {

	Stage stage;

	@Override
	public void create() {
		stage = new FieldStage(this);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();
	}

	public void switchToEndGameStage(Cell[][] cells, boolean won) {
		stage.dispose();
		stage = new EndGameStage(cells, this, won);
		Gdx.input.setInputProcessor(stage);
	}

	public void switchToFieldStage() {
		stage.dispose();
		stage = new FieldStage(this);
		Gdx.input.setInputProcessor(stage);
	}

}

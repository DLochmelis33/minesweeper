package com.dl33.boom.stages;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.dl33.boom.MinesweeperGame;
import com.dl33.boom.actors.Cell;
import com.dl33.boom.actors.Field;
import com.dl33.boom.actors.Selection;

public class FieldStage extends Stage {

	public final int x_dimension = 20;
	public final int y_dimension = 20;
	public final int minesAmount = 55;

	private Cell[][] cells;
	private boolean firstTouch = true;
	private Selection selection;
	private MinesweeperGame game;
	private int flagsAmount = 0;

	public FieldStage(MinesweeperGame game) {
		this.game = game;

		Field field = new Field();
		addActor(field);

		cells = new Cell[y_dimension][x_dimension];
		for (int i = 0; i < y_dimension; i++) {
			for (int j = 0; j < x_dimension; j++) {
				cells[i][j] = new Cell(i * 20 + 20, j * 20 + 20, i, j);
				addActor(cells[i][j]);
			}
		}

		selection = new Selection();
		selection.setVisible(false);
		addActor(selection);

	}

	void mineField(int xInit, int yInit) {
		// fills the field with mines, (xInit, yInit) cell must have value = 0
		Random r = new Random();

		int x = r.nextInt(x_dimension);
		int y = r.nextInt(y_dimension);

		int cnt = 0;
		while (cnt < minesAmount) {
			if (!cells[x][y].isMined() && (Math.abs(x - xInit) > 1 || Math.abs(y - yInit) > 1)) {
				cells[x][y].setMined(true);
				cnt++;
			} else {
				x = r.nextInt(x_dimension);
				y = r.nextInt(y_dimension);
			}
		}

	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		y = (int) (getHeight() - y);

		int xCoord = (int) (x - 20) / x_dimension;
		int yCoord = (int) (y - 20) / y_dimension;

		switch (button) {
		case 0:
			for (Cell[] j : cells) {
				for (Cell i : j) {
					if (x >= i.getX() && x <= i.getX() + 20f && y >= i.getY() && y <= i.getY() + 20f) {
						if (firstTouch) {
							mineField(i.getXCoord(), i.getYCoord());
							setupCellsValues();
							firstTouch = false;
						}

						if (xCoord < x_dimension && yCoord < y_dimension) {
							try {
								recursiveOpen(yCoord, xCoord);
							} catch (IllegalStateException e) {
								// assuming the only such exception can be
								// ExplodeException
								game.switchToEndGameStage(cells, false);
							}

						}

						return true;
					}
				}
			}
			return false;
		case 1:
			if (firstTouch) {
				return false;
			}

			if (xCoord < x_dimension && yCoord < y_dimension) {
				if (cells[xCoord][yCoord].isOpened()) {
					return false;
				}
				if (cells[xCoord][yCoord].toggleMark()) {
					flagsAmount++;
				} else {
					flagsAmount--;
				}

				// checking if the game is won
				boolean flag = false;
				for (int k = 0; k < y_dimension; k++) {
					for (int l = 0; l < x_dimension; l++) {
						if (cells[k][l].isMined() && !cells[k][l].isMarked()) {
							flag = true;
							break;
						}
					}
					if (flag) {
						break;
					}
				}
				if (!flag && flagsAmount == minesAmount) {
					game.switchToEndGameStage(cells, true);
				}
				return true;
			}
			return false;

		default:
			return false;
		}
	}

	public void recursiveOpen(int xCoord, int yCoord) {
		if (cells[yCoord][xCoord].isOpened() || cells[yCoord][xCoord].isMarked()) {
			return;
		}
		if (cells[yCoord][xCoord].getValue() != 0) {
			cells[yCoord][xCoord].open();
			return;
		}
		cells[yCoord][xCoord].open();

		try {
			if (!cells[yCoord - 1][xCoord - 1].isOpened())
				recursiveOpen(xCoord - 1, yCoord - 1);
		} catch (Exception e) {
		}
		try {
			if (!cells[yCoord - 1][xCoord].isOpened())
				recursiveOpen(xCoord, yCoord - 1);
		} catch (Exception e) {
		}
		try {
			if (!cells[yCoord - 1][xCoord + 1].isOpened())
				recursiveOpen(xCoord + 1, yCoord - 1);
		} catch (Exception e) {
		}
		try {
			if (!cells[yCoord][xCoord - 1].isOpened())
				recursiveOpen(xCoord - 1, yCoord);
		} catch (Exception e) {
		}
		try {
			if (!cells[yCoord][xCoord + 1].isOpened())
				recursiveOpen(xCoord + 1, yCoord);
		} catch (Exception e) {
		}
		try {
			if (!cells[yCoord + 1][xCoord - 1].isOpened())
				recursiveOpen(xCoord - 1, yCoord + 1);
		} catch (Exception e) {
		}
		try {
			if (!cells[yCoord + 1][xCoord].isOpened())
				recursiveOpen(xCoord, yCoord + 1);
		} catch (Exception e) {
		}
		try {
			if (!cells[yCoord + 1][xCoord + 1].isOpened())
				recursiveOpen(xCoord + 1, yCoord + 1);
		} catch (Exception e) {
		}

		// y -> x
		// x -> invert -> y
	}

	public void setupCellsValues() {

		// try to fix as only OOB needs to be intercepted

		for (int j = 0; j < y_dimension; j++) {
			for (int i = 0; i < x_dimension; i++) {

				int bombCnt = 0;

				try {
					if (cells[i - 1][j - 1].isMined())
						bombCnt++;
				} catch (Exception e) {
				}
				try {
					if (cells[i - 1][j].isMined())
						bombCnt++;
				} catch (Exception e) {
				}
				try {
					if (cells[i - 1][j + 1].isMined())
						bombCnt++;
				} catch (Exception e) {
				}
				try {
					if (cells[i][j - 1].isMined())
						bombCnt++;
				} catch (Exception e) {
				}
				try {
					if (cells[i][j + 1].isMined())
						bombCnt++;
				} catch (Exception e) {
				}
				try {
					if (cells[i + 1][j - 1].isMined())
						bombCnt++;
				} catch (Exception e) {
				}
				try {
					if (cells[i + 1][j].isMined())
						bombCnt++;
				} catch (Exception e) {
				}
				try {
					if (cells[i + 1][j + 1].isMined())
						bombCnt++;
				} catch (Exception e) {
				}

				cells[i][j].setValue(bombCnt);
			}
		}
	}

	@Override
	public boolean mouseMoved(int x, int y) {
		y = (int) (getHeight() - y);

		if (x >= 20 && x < getWidth() - 20 && y >= 20 && y < getHeight() - 20) {
			selection.setX(x - x % 20);
			selection.setY(y - y % 20);
			selection.setVisible(true);
			return true;
		} else {
			selection.setVisible(false);
		}
		return false;
	}

}

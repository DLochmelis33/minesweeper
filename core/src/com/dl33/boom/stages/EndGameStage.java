package com.dl33.boom.stages;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.dl33.boom.MinesweeperGame;
import com.dl33.boom.actors.Cell;
import com.dl33.boom.actors.Field;
import com.dl33.boom.actors.PlayAgainButton;

public class EndGameStage extends Stage {

	PlayAgainButton pab = new PlayAgainButton(0, 0, false);
	MinesweeperGame game;

	public EndGameStage(Cell[][] cells, MinesweeperGame game, boolean won) {
		addActor(new Field());
		this.game = game;

		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				cells[i][j].dissecure();
				addActor(cells[i][j]);
			}
		}

		pab = new PlayAgainButton((int) (getViewport().getWorldWidth() - pab.getWidth()) / 2,
				(int) (getViewport().getWorldHeight() - pab.getHeight()) / 2, won);
		addActor(pab);

	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		if (x > pab.getX() && x < pab.getX() + pab.getWidth() && y > pab.getY() && y < pab.getY() + pab.getHeight()) {
			pab.setVisible(false);
			game.switchToFieldStage();
			return true;
		}
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		if (screenX > pab.getX() && screenX < pab.getX() + pab.getWidth() && screenY > pab.getY()
				&& screenY < pab.getY() + pab.getHeight()) {
			pab.highlight();
			return true;
		}
		pab.lowlight();
		return false;
	}

}

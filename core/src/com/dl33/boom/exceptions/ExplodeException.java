package com.dl33.boom.exceptions;

public class ExplodeException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExplodeException() {
		super("You lost!");
	}

}

package com.dl33.boom.exceptions;

public class WinException extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WinException() {
		super("You won!");
	}

}

package com.dl33.boom.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.dl33.boom.MinesweeperGame;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 440;
		config.height = 440;
		config.resizable = false;
		new LwjglApplication(new MinesweeperGame(), config);
	}
}
